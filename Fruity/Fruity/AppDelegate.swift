//
//  AppDelegate.swift
//  Fruity
//
//  Created by JACOB BORG on 2/8/20.
//  Copyright © 2020 Jacob Borg. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

