//
//  ViewController.swift
//  Fruity
//
//  Created by JACOB BORG on 2/8/20.
//  Copyright © 2020 Jacob Borg. All rights reserved.
//

import Cocoa
import WebKit
import Foundation

class SFContentBlockerManager : NSObject{}
class SFContentBlockerState : NSObject{
    
    var isEnabled: Bool { true }
    
}

class ViewController: NSViewController, NSApplicationDelegate, WKNavigationDelegate, WKUIDelegate, NSTextFieldDelegate{

    @IBOutlet var window: NSView!
    @IBOutlet weak var forwardButton: NSButton!
    @IBOutlet weak var backButton: NSButton!
    @IBOutlet weak var webView: WKWebView!

    @IBOutlet weak var label: NSTextField!
    
    

    
    @IBOutlet weak var refreshButton: NSButton!
    
    
    @IBAction func back(_ sender: Any){
        if webView.canGoBack{
            webView.goBack()
        }
    }
    
    @IBAction func next(_ sender: Any){
        if webView.canGoForward{
            webView.goForward()
        }
    }
    
    @IBAction func refresh(_ sender: Any){
        
        webView.reload()
    }
    @IBAction func loadHome(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/page2.html")!))
    }

    @IBAction func fruityHelp(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/fruityhelp.html")!))
    }
    @IBAction func terms(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://docs.google.com/document/d/1i7_xhGHgM4Nac9GvYtealhly4Dx5-E0iEdcDeg_GVkM/edit?usp=sharing")!))
    }
    @IBAction func supportus(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.paypal.com/biz/fund?id=HZ6CMQARKJQBN")!))
    }
    
    
    @IBAction func soundcloud(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://soundcloud.com/")!))
    }
    @IBAction func pandora(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "http://player.listenlive.co/57091")!))
    }
    @IBAction func klove(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "http://www.klove.com/listen/player.aspx")!))
    }
    @IBAction func klovechristmas(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://listen.klove.com/christmas")!))
    }
    @IBAction func iheart(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://listen.air1.com/")!))
    }
    @IBAction func shazam(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://listen.air1.com/christmas")!))
    }

    
    
    
    @IBAction func google(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.google.com/")!))
    }
    @IBAction func startpage(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.startpage.com/")!))
    }
    @IBAction func bing(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.bing.com/")!))
    }

    @IBAction func bcssearch(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://search.borgcreative.com/")!))
    }
    
    
     @IBAction func ClearOnQuit(_ sender: Any) {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
                
            }
            
        }
        NSApplication.shared.terminate(self)
    }
    
    

    
    @IBAction func clear(_ sender: Any){
       webView.load(URLRequest(url: URL(string: "https://dev.jacobborgprogramming.net/homepage.html")!))
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
                
            }
            
        }

               webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/page2.html")!))
        
    }

    @IBAction func deleteCookies(_ sender: Any){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
        webView.reload()
    }


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
 
        

        webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/page2.html")!))
        
        
        
    }

    
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
            
            
        }
    }


}

