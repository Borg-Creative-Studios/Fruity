//
//  ViewController.swift
//  Fruity
//
//  Created by JACOB BORG on 2/8/20.
//  Copyright © 2020 Jacob Borg. All rights reserved.
//

import Cocoa
import WebKit
import Foundation

class SFContentBlockerManager : NSObject{}
class SFContentBlockerState : NSObject{
    var isEnabled: Bool { true }
    
}

class ViewController: NSViewController, NSApplicationDelegate, WKNavigationDelegate, WKUIDelegate, NSTextFieldDelegate{

    @IBOutlet var window: NSView!
    @IBOutlet weak var forwardButton: NSButton!
    @IBOutlet weak var backButton: NSButton!
    @IBOutlet weak var webView: WKWebView!

    
    @IBOutlet weak var refreshButton: NSButton!
    

    
    @IBAction func back(_ sender: Any){
        if webView.canGoBack{
            webView.goBack()
        }
    }
    
    @IBAction func next(_ sender: Any){
        if webView.canGoForward{
            webView.goForward()
        }
    }
    
    @IBAction func refresh(_ sender: Any){
        
        webView.reload()
    }
    @IBAction func loadHome(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/2-3-0.html")!))
    }
    @IBAction func fruityHelp(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://support.borgcreative.com/")!))
    }
    @IBAction func terms(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/assets/files/Terms-andConditions-Fruity.pdf")!))
    }
    
    @IBAction func bcsauthor(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "http://author.borgcreative.com/")!))
    }
    @IBAction func facebook(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.facebook.com")!))
    }
    @IBAction func instagram(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.instagram.com")!))
    }
    @IBAction func twitter(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.twitter.com")!))
    }
    @IBAction func telegram(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://telegram.org")!))
    }
    @IBAction func tumblr(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.tumblr.com")!))
    }
    @IBAction func whatsapp(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.whatsapp.com/")!))
    }
    @IBAction func blogger(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.blogger.com")!))
    }
    @IBAction func wordpress(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://wordpress.com/")!))
    }
    
    
    @IBAction func CNN(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.cnn.com")!))
    }
    @IBAction func MSNBC(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.msnbc.com")!))
    }
    @IBAction func Foxnews(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.foxnews.com")!))
    }
    @IBAction func Yahoonews(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://news.yahoo.com")!))
    }
    @IBAction func churchMetrics(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://churchmetrics.com")!))
    }
    @IBAction func odb(_ sender: Any){
                    webView.load(URLRequest(url: URL(string: "https://odb.org")!))
        }
    
    
    @IBAction func Bible(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.bible.com")!))
    }
    @IBAction func BibleGateway(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.biblegateway.com")!))
    }
    @IBAction func wdm(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.wavedm.net")!))
    }
    @IBAction func planning(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://planning.center")!))
    }
    @IBAction func errorReport(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://support.borgcreative.com/supportticket.html")!))
    }
    
    
    
    @IBAction func google(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.google.com/")!))
    }
    @IBAction func startpage(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.startpage.com/")!))
    }

    @IBAction func bing(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.bing.com/")!))
    }
    @IBAction func verified(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://dev.jacobborgprogramming.net/fruityverify.html")!))
    }
    @IBAction func bcssearch(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://search.borgcreative.com/")!))
    }
    @IBAction func onesearch(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.onesearch.com")!))
    }
    @IBAction func fastcom(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://fast.com")!))
    }
    @IBAction func speedtest(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.speedtest.net")!))
    }
    @IBAction func store(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://teespring.com/stores/borgcreative")!))
    }
    @IBAction func marketing(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://www.borgcreative.com/page12.html")!))
    }
    
    @IBAction func cloud(_ sender: Any){
                webView.load(URLRequest(url: URL(string: "https://cloud.borgcreative.com/my/")!))
    }
    
    
     @IBAction func ClearOnQuit(_ sender: Any) {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
                
            }
            
        }
        NSApplication.shared.terminate(self)
    }
    
    

    
    @IBAction func clear(_ sender: Any){
      //  webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/2-3-0.html")!))
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
                
            }
            
        }

               webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/2-3-0.html")!))
        
    }


    @IBAction func deleteCookies(_ sender: Any){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
        webView.reload()
    }


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
 
        

        webView.load(URLRequest(url: URL(string: "https://borg-creative-studios.github.io/fruity-website/2-3-0.html")!))
        
        
        
    }

    
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
            
            
        }
    }


}

