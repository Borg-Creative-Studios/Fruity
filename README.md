# fruity
A new, privacy-friendly, minimalist browser for MacOS.
Fruity supports MacOS 11.00 and newer.

Fruity features three build channels, Kiwi, Tangerine, and Lemon.
## Kiwi
Kiwi is the free, stable version of Fruity (Includes homepage ads).

## Tangerine
Tangerine is the free, unstable/beta version of Fruity (Includes homepage ads).

## Lemon
Lemon is the paid, stable version of Fruity with a few unique features and no homepage ads.
We charge for Lemon as a way to help fund the project as software development is not free.

Please consider buying Fruity Lemon.
https://gamejolt.com/games/fruity/629125